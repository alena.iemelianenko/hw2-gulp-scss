import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

let headerMenuIcon = document.querySelector(".header__icon");
let headerMenuIconActive = document.querySelector(".header__icon--active");
let headerMenuIconNotActive = document.querySelector(".header__icon--not-active");
let headerMenu = document.querySelector(".header__menu");


headerMenuIcon.addEventListener('click', (e) =>  {
    if (e.target == headerMenuIconActive) {
        headerMenu.style.display="block";
        headerMenuIconActive.style.display="none";
        headerMenuIconNotActive.style.display="block";
    } else {
        headerMenu.style.display="none";
        headerMenuIconActive.style.display="block";
        headerMenuIconNotActive.style.display="none";
    }
});

